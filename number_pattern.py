def print_num_pattern(num1: int, num2: int) -> None:
    # Base case
    if num1 <= 0:
        print(num1, end=" ")
        return

    # Output decreasing order first
    print(num1, end=" ")
    print_num_pattern(num1 - num2, num2)

    # Output increasing order
    print(num1, end=" ")


if __name__ == "__main__":
    num1: int = int(input())
    num2: int = int(input())
    print_num_pattern(num1, num2)
