# Introduction

Write a recursive fuction, print_num_pattern(), in the number_pattern.py program
to output a various sequence of numbers.

This sequence is defined by two inputted, positive integers. The first being a
starting and ending point, more on this, keep reading. The second being a decrement
counter (still positive input btw). Using these two numbers, use the decrement integer
to continuously subract until 0 or a negative number is reached. After this capstone
has been achieved, then add the decrement to the now 0 or negative number to the
value stored in the first inputted integer.

Examples below:

Input would look like this:
```
12 # user inputted start and end point
3 # user inputted decrement
```

Output would look like this:
```
12 9 6 3 0 3 6 9 12 
```

Another example:
```
14 # both inputs again
5
```

Output would look like:
```
14 9 4 -1 4 9 14
```

This goes for any output.

Another note, there is whitespace behind every number printed, including
the last one. Just to avoid any worries.

All you need to do is program the print_num_pattern(), don't worry about anything
else!

Good luck and have fun!