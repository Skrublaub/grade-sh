#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys

# Temporarily add the current path to the system path for importing the student's source code.
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))), ".admin_files"
    )
)
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# python3 seemingly respects only abspaths, while ipython3 is ok with relative, like '..' here.
import test_utils

from number_pattern import print_num_pattern

class WritesToString(object):
    def __init__(self) -> None:
        self.string: str = ""

    def write(self, string: str) -> None:
        self.string += string

@test_utils.test_wrapper
def test() -> bool:
    result: bool = False
    # Backup sys.stdout before changing
    stdoutBackup = sys.stdout
    
    # Redirect std I/O
    writer = WritesToString()
    sys.stdout = writer
    
    num1: int = 2
    num2: int = 1
    
    # Call print_num_pattern()
    print_num_pattern(num1, num2)
    
    # Restore sys.stdout
    sys.stdout = stdoutBackup
    
    # Get program output and expected output
    programOutput = writer.string
    expectedOutput: str = "2 1 0 1 2 "
    
    # Compare program output to expected
    if programOutput == expectedOutput:
        result = True
    
    return result

if __name__ == "__main__":
    test()